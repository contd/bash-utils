#!/usr/bin/env bash

# Git Additions
alias gco="git checkout"
# Yarn
alias yga="yarn global add --prefix ~/.local"
# Atom tweaks
#alias atom='tmpin atom'
#alias gda='git diff | tmpin atom'

